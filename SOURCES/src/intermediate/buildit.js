//  Must pass 3 arguments to this script:
//  	valuesfile is a json document with all the needed values
//  	scriptfile is the target location of the generated sh.script
//  	wrapperfile is the target location of the generated wrapper
//
//  	If this fails to write, we don't care
importPackage(java.io);
var loc="/usr/share/java-tanuki-wrapper/src/intermediate"
load(loc + "/mustache.js", loc+"/Yaml.js", loc + "/YamlParser.js", loc + "/YamlParseException.js", loc + "/YamlInline.js", loc + "/YamlUnescaper.js");
function writeit(template,target,values) {
	try {
		var t = readFile(template);
		var o = new File(target);
		var w = new FileWriter(o);
		var rendered = Mustache.render(t,values);
		w.write("# File created for JSW \n");
		w.write(rendered);
		w.write("\n");
		w.close()
	} catch(e) {
		print("Sorry, we failed to write");
	}
}

var valuesfile = arguments[0]
//if (new java.lang.String(valuesfile).endsWith("yaml")) {
//print "yaml";
//
//if (new java.lang.String(valuesfile).endsWith("json")) {
//print "json";
//
//uit()
var scriptfile = arguments[1]
var wrapperfile = arguments[2]
var v = readFile(valuesfile);
//var values = JSON.parse(v);
var values = YAML.parse(v);
// print(values.app_wrapper_tanukihome);
//quit()
writeit("/usr/share/java-tanuki-wrapper/src/bin/sh.script.in.mustache",scriptfile,values);
writeit("/usr/share/java-tanuki-wrapper/src/conf/wrapper.conf.in.mustache",wrapperfile,values);
