Here are the instructions on how to use the intermediate wrapper RPM

Wrapping the JSW into an RPM is very simple.  Making a script that will actually work with
the install of a java application is the hard part.  So you have two options:
First option (do it all yourself): 
	Your spec file just needs to set the correct location of ALL the params for the given install of JSW.
	This means you should take a copy of the files from your local install of the intermediate package and
		build your wrapper.conf and sh.script files from that
Second option (write some yaml and let your install build the config files):
	There are some example spec files in the installation that do this.  They generate a yaml file that is then
		read using some javascript (and Rhino) and interpreted using Mustache to produce the correctly
		filled out templatized wrapper and script.  All you really need to do is provide the locations
		for these things and change the appropriate values in the example.

	IMO, this is way easier once you've gotten the hang of it all.  

The second path is what is provided with this package.  Inside your package's .spec, you'll need to generate
a YAML file as part of the %post that includes all the information needed to fill out the JSW Mustache templates.
Some attributes in the YAML are optional (and are commented out below).
Currently, that file looks like :

---
app_wrapper_tanukihome : "/usr/share/java-tanuki-wrapper"
app_name : APP_NAME
app_long_name : APP_LONG_NAME
app_description : long description of APP_NAME
# app_run_as_user : root
#app_wrapper_sourced_files :	# This list of file is sourced in the script prior to execution
#  - file : file1.sh
#  - file : /etc/file2.sh
app_wrapper_conf_location: RELATIVE_OR_ABSOLUTE_PATH_TO_GENERATED_CONF
app_priority : fakepriority
app_piddir : .
# app_fixed_command : CONSOLE
# app_pass_through : TRUE
# app_ignore_signals : TRUE
# app_wait_after_startup : 0
# app_wait_for_started_status : TRUE
# app_wait_for_started_timeout : 120
# app_detail_status : TRUE
# app_pausable : TRUE
# app_brief_usage : TRUE
# app_use_upstart : TRUE
# app_use_systemd : TRUE
# app_plist_domain : org_tanukisoftware_wrapper
app_chkconfig_level : 345
app_chkconfig_start_order : 95
app_chkconfig_stop_order : 05
app_suse_default_start : 95
app_suse_default_stop : 05
# app_runlevel : 20
#app_wrapper_auto_bits : TRUE
#app_wrapper_console_format : PM
#app_wrapper_console_loglevel : INFO
#app_wrapper_encoding : UTF-8
#app_wrapper_ignore_seq_gaps : TRUE
#app_wrapper_include_files :	# Included in the wrapper.conf file
#  - file : somefile
#  - file : anotherfile
#app_wrapper_java_additionals :	# Additional Java parameters
#  - item : 1=someparam
#  - item : 2=anotherparam
#app_wrapper_java_classpaths:	# Additional java classpaths (beyond wrapper.jar)
#  - item : 2=someparam
#  - item : 3=anotherparam
#app_wrapper_parameters :	# Additional wrapper params
#  - item : 2=someparam
#  - item : 3=anotherparam
#app_wrapper_java_command: %JAVA_HOME%/bin/java
#app_wrapper_java_library_paths: #Additional Java library paths beyond the current one
#  - item:  2=somepath
#  - item:  3=somepath
#app_wrapper_lang: en_US
#app_wrapper_lang_folder: ../lang
#app_wrapper_logfile: ../logs/wrapper.log
#app_wrapper_logfile_format: LPTM
#app_wrapper_logfile_loglevel: INFO
#app_wrapper_logfile_maxfiles: 0
#app_wrapper_logfile_maxsize: 0
#app_wrapper_mainclass: org.tanukisoftware.wrapper.WrapperSimpleApp	# The JSW's main function (
#app_wrapper_pidfile_strict:
#app_wrapper_syslog_loglevel:
app_wrapper_java_home: /usr/java/default	# You REALLY need to set this correctly
app_wrapper_java_initmemory: 3
app_wrapper_java_maxmemory: 64
app_wrapper_mymain: com.example.mymain		# Your app's main function
app_wrapper_loglevel: INFO
#app_wrapper_ntserver_dependencies:		# This is stuff listed as NT Dependencies.  If you're using JSW on windows, you probably know what this is
#  - item: 1=somentdependency
#  - item: 2=someotherntdependency
