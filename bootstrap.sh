# Future bootstrapping script
for BITS in 32 64
do
	VERSION=3.5.25
	NAME="wrapper-linux-x86-${BITS}-${VERSION}.tar.gz"
	SRC="http://wrapper.tanukisoftware.com/download/${VERSION}/${NAME}"
	curl ${SRC} -o ./SOURCES/${NAME}
done
