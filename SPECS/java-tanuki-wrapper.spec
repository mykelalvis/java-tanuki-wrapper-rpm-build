%define 	debug_package 	%{nil}
%define 	__jar_repack 	%{nil}

Name:           java-tanuki-wrapper
Version:        3.5.25
Release:        1

%define		_this_id	wrapper-linux-x86-%{archbits}-%{version}
%define		_this_home	/usr/share/%{name}
%define		_this_user	root
%define		_this_group	root
%ifarch i386
%define		archbits	32
%endif
%ifarch x86_64
%define		archbits	64
%endif

%define		app_name	%{name}
%define		app_long_name	%{name} Tanukisoft Wrapper intermediate instance build

License:        GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
Group:          Development/BuildTools
Summary:        Java Service Wrapper
Source0:        http://wrapper.tanukisoftware.com/download/%{version}/%{_this_id}.tar.gz
Source1:        %{name}-intermediate-%{version}.tar.gz
BuildRequires:  tar 
# Removed requirement for rhino by directly including the rhino jar (which was probably unnecessary)
Requires:	jdk >= 1.6
Packager: 	%{packager}
# Need to change the direct inclusion of the altered wrapper.conf and sh.script files to a patch (maybe?)
# Patch0:		%{name}.patch

%description
The Tanukisoft Java Service Wrapper is an excellent tool for starting Java applications and keeping them running.
This package is an intermediate layer, providing a templating mechanism that allows developers to write
a simplified spec file that utilizes this package as a means for producing a templatized run script and
wrapper.conf.  Essentially, a developer can produce a short yaml file that is used by Mustache to
automatically generate the wrapper.conf and run script for a JSW-enabled application, reusing the
JSW installation and templates multiple times.

Documentation for the wrapper.conf file is available at http://wrapper.tanukisoftware.com/
Documentation on how to use this RPM is in the README_en.txt at /usr/share/%{name}/doc

%prep
%setup -n jsw -c -T -b 0
%setup -D -n jsw/%{_this_id} -T -a 1 
# %{__cp} src/conf/wrapper.conf.in src/conf/wrapper.conf.orig
# %{__cp} src/bin/sh.script.in src/bin/sh.script.orig
%{__rm} -rf doc/* jdoc jdoc.* logs conf bin/demoapp bin/testwrapper lib/wrapperdemo.jar lib/wrappertest.jar README_es.txt README_ja.txt README_de.txt
%{__mv} README_en.txt doc
%{__mv} src/README_intermediate.txt doc

%preun

%install
# Make the build root targets
install -d $RPM_BUILD_ROOT/usr/share/%{name}
# And copy the whole thing into them.  All the real work happens in the secondary RPM installs
cp -R * $RPM_BUILD_ROOT/usr/share/%{name}

%clean
rm -rf "$RPM_BUILD_ROOT"

%post

%postun

%files
%defattr(-,%{_this_user},%{_this_group})
%doc /usr/share/%{name}/doc
/usr/share/%{name}/

%changelog
* Fri Sep 12 2014 Mykel Alvis <mykel.alvis@gmail.com>
- Update to 3.5.25

* Wed Aug 03 2011 Mykel Alvis <mykel.alvis@gmail.com>
- Created first rpm for b-flat installation of Tanuki Wrapper

