%define 	debug_package 	%{nil}
%define 	__jar_repack 	%{nil}

Name:           generic
Version:        1.0.0
Release:        1

Prefix:		/opt

# Locations.  This is sort of contrived, but it works
%define		_this_id		%{_this_name}-%{version}
%define		_this_home		%{prefix}/%{name}-%{version}

# Define users and groups for app
%define		_this_user		generic
%define		_this_uid		527
%define		_this_group		generic
%define		_this_gid		527

%define		_this_user_home		%{_this_home}
%define		_this_service_name	%{name}-

# These ports are used to generate instances in /var/opt/%{name} as HTTPPORT:SSLPORT:AJPPORT:STOPPORT:JMXPORT
%define		_portlist		8080:8443:8009:8005:8006 9000:9001:9002:9003:9004 9010:9011:9012:9013:9014 9020:9021:9022:9023:9024 9030:9031:9032:9033:9034 9040:9041:9042:9043:9044 9050:9051:9052:9053:9054 9060:9061:9062:9063:9064 9070:9071:9072:9073:9074 9080:9081:9082:9083:9084

License:        Apache License v2.0 http://www.apache.org/licenses
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
Group:          External/Generic
Summary:        This is the version of apache installed in /opt with instances in /var/opt/%{_this_name}.  It requires jdk >= 1.6
Source:         http://www.gtlib.gatech.edu/pub/apache/tomcat/tomcat-6/v%{version}/bin/%{name}-%{version}.zip
BuildArch:      noarch
BuildRequires:  unzip
Requires(pre):	shadow-utils java-tanuki-wrapper >= 3.5.25
Requires:	jdk >= 1.6 java-tanuki-wrapper >= 3.5.25
Packager: 	%{packager}
# Patch0:		%{name}.0

%description
Generic application, installed from binary zip into /opt as '%{_this_user}' 

%prep
%setup -n %{name}-%{version}

# produce or acquire the yaml for the values
# determine the targets for the wrapper and script


%pre
getent group %{_this_group} >/dev/null || groupadd -r %{_this_group} --gid %{_this_gid}
getent passwd %{_this_user} >/dev/null || useradd -r -g %{_this_group} -d %{_this_home} -s /bin/bash -c "EXAMPLE User" --uid %{_this_uid} %{_this_user}
install --group=%{_this_group} --owner=%{_this_user} -d $RPM_BUILD_ROOT/%{_this_home}

%preun

%build

%install
install -d $RPM_BUILD_ROOT/%{_this_home}
install -d $RPM_BUILD_ROOT/var/opt/%{name}
install -d $RPM_BUILD_ROOT/var/log/%{name}
install -d $RPM_BUILD_ROOT/var/run/%{name}/
cp -R * $RPM_BUILD_ROOT/%{_this_home}

#install -p bin $RPM_BUILD_ROOT/opt/%{name}-%{version}/bin
install -d $RPM_BUILD_ROOT/etc
install -d $RPM_BUILD_ROOT/etc/init.d
install -d $RPM_BUILD_ROOT/var/log/%{name}
install -d $RPM_BUILD_ROOT/var/run/%{name}

for p in %{_portlist}
do

set -- "$p" 
IFS=":"; declare -a Array=($*) 
PORTS="${Array[@]}" 
PORT_HTTP="${Array[0]}" 
PORT_SSL="${Array[1]}" 
PORT_AJP="${Array[2]}" 
PORTSTOP="${Array[3]}" 
PORTJMX="${Array[4]}" 

DIR=/var/opt/%{name}/%{name}-$PORT_HTTP
install -d $RPM_BUILD_ROOT/var/opt/%{name}/%{name}-$PORT_HTTP
cp -R bin conf temp webapps work $RPM_BUILD_ROOT/var/opt/%{name}/%{name}-$PORT_HTTP
mkdir $RPM_BUILD_ROOT/var/opt/%{name}/%{name}-$PORT_HTTP/locallib
mkdir $RPM_BUILD_ROOT/var/opt/%{name}/%{name}-$PORT_HTTP/locallib/classes


for a in $RPM_BUILD_ROOT/var/opt/%{name}/%{name}-$PORT_HTTP/bin/initscript.sh $RPM_BUILD_ROOT/var/opt/%{name}/%{name}-$PORT_HTTP/conf/wrapper.conf $RPM_BUILD_ROOT/var/opt/%{name}/%{name}-$PORT_HTTP/conf/server.xml $RPM_BUILD_ROOT/var/opt/%{name}/%{name}-$PORT_HTTP/conf/logging.properties
do

install -d $RPM_BUILD_ROOT/var/log/%{name}/%{name}-${PORT_HTTP}


# these items have to be re-processed again into multiple copies for the /var/opt/apache-tomcat-XXXX items
%{__sed} --in-place -e"s/@app.long.name@/%{name}-${PORT_HTTP}/" $a
%{__sed} --in-place -e"s/@app.long.name@/%{name}-${PORT_HTTP}/" $a
%{__sed} --in-place -e"s/@app.name@/%{name}-${PORT_HTTP}/" $a
%{__sed} --in-place -e"s/@app.description@/%{name} running on ${PORT_HTTP}/" $a
%{__sed} --in-place -e"s/@app.wrapper.logfile@/\/var\/log\/%{name}\/%{name}-${PORT_HTTP}\/wrapper.log/" $a
%{__sed} --in-place -e"s/@app.wrapper.conf@/..\/conf\/wrapper.conf/" $a
%{__sed} --in-place -e"s/@app.tomcat.http@/${PORT_HTTP}/" $a
%{__sed} --in-place -e"s/@app.tomcat.port@/${PORT_HTTP}/" $a
%{__sed} --in-place -e"s/@app.tomcat.ssl@/${PORT_SSL}/" $a
%{__sed} --in-place -e"s/@app.tomcat.ajp@/${PORT_AJP}/" $a
%{__sed} --in-place -e"s/@app.tomcat.jmx@/${PORTJMX}/" $a
%{__sed} --in-place -e"s/@app.tomcat.stop@/${PORTSTOP}/" $a
%{__sed} --in-place -e"s/@app.varopt.home@/\/var\/opt\/%{name}\/%{name}-${PORT_HTTP}/" $a

done

mv $RPM_BUILD_ROOT/var/opt/%{name}/%{name}-$PORT_HTTP/bin/initscript.sh $RPM_BUILD_ROOT/var/opt/%{name}/%{name}-$PORT_HTTP/bin/tomcat-${PORT_HTTP}.init.sh

done


%clean
rm -rf "$RPM_BUILD_ROOT"

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,%{_this_user},%{_this_group})
%{_this_home}
/var/opt/%{name}
/var/run/%{name}
/var/log/%{name}


%changelog
* Tue Sep 14 2011 Mykel Alvis <mykel.alvis@gmail.com>
- Fifth revision sets up JMX ports and puts an abbreviated copy of tomcat-users.xml into conf for the instance and webapps.held in the base install dir for copying to instances
* Tue Sep 14 2011 Mykel Alvis <mykel.alvis@gmail.com>
- Fourth revision adds locallib and locallib/classes to the running classpath
* Tue Aug 09 2011 Mykel Alvis <mykel.alvis@gmail.com>
- Second Revision of 6.0.32, using java-tanuki-wrapper for init scripts
* Thu Mar 31 2011 Mykel Alvis <mykel.alvis@gmail.com>
- First Revision of 6.0.32

