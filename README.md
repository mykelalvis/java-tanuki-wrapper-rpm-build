java-tanuki-wrapper-rpm-build

Look to the generic.spec file to see how this works.  The JSW is installed (at /usr/share/java-tanuki-wrapper) and has some additional code (YAML and Mustache processing) to perform the templating

Inside your spec file, you have the spec generate a YAML file for template variables.  In most cases, you'll know these at packaging time and at others you'll need to generate the file after the install.  In any case, there's a script that gets generated to produce the init.sh and the wrapper.conf files for your code.

THIS SOFTWARE DOES NOT FILL IN THE JSW VALUES FOR YOU!  It just makes it simpler to produce a working Java/JSW-based RPM.

Mykel


